const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const graphqlHTTP = require('express-graphql');

const url = 'mongodb://localhost:27017/myappdb';

const connect = mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true});
connect.then((db) => {
    console.log('Connected to Mongodb Successfully');
}, (err) => {
    console.log(err);
})

const userSchema = require('./graphql/index').userSchema;
app.use('/graphql', cors(), graphqlHTTP({
    schema: userSchema,
    rootValue: global,
    graphiql: true
  }));

app.use(bodyParser.json());
app.use('*', cors());

app.listen({port: 5000}, () => {
    console.log(`🚀 Server ready at http://localhost:5000`);
});

