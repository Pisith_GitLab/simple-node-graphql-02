const { GraphQLObjectType, GraphQLNonNull, GraphQLID, GraphQLString } = require('graphql');

// User Type
exports.userType = new GraphQLObjectType({
    name: 'user',
    fields: () => {
        return {
            id: {
                type: new GraphQLNonNull(GraphQLID)
            },
            name: {
                type: GraphQLString
            }
        }
    }
});