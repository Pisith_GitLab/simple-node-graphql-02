const { GraphQLObjectType, GraphQLList } = require('graphql');
const userModel = require('../../models/user');
const userType = require('../types/user').userType;

// User Query
exports.getUsers = new GraphQLObjectType({
    name: 'Query',
    fields: () => {
        return {
            users: {
                type: new GraphQLList(userType),
                resolve: () => {
                    const users = userModel.find().exec();
                    if (!users) { throw new Error('Error') }
                    return users
                }
            }
        }
    }
});
