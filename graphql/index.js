const { GraphQLSchema, GraphQLObjectType } = require('graphql');
const query = require('./queries/index');
const mutation = require('./mutations/index');

exports.userSchema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: query
    }),
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: mutation
    })
})