const { GraphQLNonNull, GraphQLString } = require('graphql');
const userType = require('../types/user').userType;
const userModel = require('../../models/user');

exports.add = {
    type: userType,
    args: {
        name: {
            type: new GraphQLNonNull(GraphQLString),
        }
    },
    resolver(root, params) {
        const uModel = new userModel(params);
        const newUser = uModel.save();
        if (!newUser) { throw new Error('Error') }
        return newUser;
    }
}