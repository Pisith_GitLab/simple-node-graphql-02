const mongooose = require('mongoose');
const Schema = mongooose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true
    }
});

const User = mongooose.model('User', UserSchema);
module.exports = User;